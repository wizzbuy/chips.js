/*
 * chips.js -- http://bitbucket.org/wizzbuy/chips.js
 * Copyright 2014, Acute Analytics <bmyard@acute.fr>.
 * Licensed under the ISC license.
 */

(function (window, $) {

  'use strict';

  /*
   * Chips public class definition
   * -----------------------------
   */

  function Chips (el, opts) {
    var self = this;

    this.$el = $(el);
    this.opts = $.extend({}, Chips.DEFAULTS, opts);

    self.bind();

    if (this.opts.auto) {
      this.auto();
    }
  }

  Chips.DEFAULTS = {
    // Cookie used to store the user's consent.
    cookie: 'cookies-consent',

    // Cookie consent expiration delay in days.
    expires: 365,

    // Automatically show after instantiation.
    auto: true,

    // Delay before automatically showing.
    delay: 750,

    // Dismiss selector.
    dismiss: '[data-dismiss=chips]',

    // jQuery options for showing.
    showDuration: 150,
    showEasing: 'linear',
    showMethod: 'slideDown',

    // jQuery options for hiding.
    hideDuration: 50,
    hideEasing: 'linear',
    hideMethod: 'slideUp',
  };

  Chips.prototype.show = function (callback) {
    this.$el[this.opts.showMethod](this.opts.showDuration,
        this.opts.showEasing, callback);
  };

  Chips.prototype.hide = function (callback) {
    this.$el[this.opts.hideMethod](this.opts.hideDuration,
        this.opts.hideEasing, callback);
  };

  Chips.prototype.bind = function () {
    this.$el.on('click', this.opts.dismiss, $.proxy(this.dismiss, this));
  };

  Chips.prototype.auto = function () {
    var self = this;

    if (this.consent()) {
      return;
    }

    setTimeout(function () {
      self.show();
    }, this.opts.delay);
  };

  Chips.prototype.dismiss = function () {
    var self = this;

    this.hide(function () {
      self.accept();
    });
  };

  Chips.prototype.consent = function () {
    return !!$.cookie(this.opts.cookie);
  };

  Chips.prototype.accept = function () {
    $.cookie(this.opts.cookie, true, {
      expires: this.opts.expires,
      path: '/'
    });
  };

  /*
   * Chips plugin definition
   * ---------------------------
   */

  function Plugin (arg) {
    return this.each(function () {
      var $this = $(this);

      // Get instance on element.
      var instance = $this.data('chips');

      // If none is found.
      if (!instance) {
        // Prepare options.
        var options = typeof arg === 'object' && arg;

        // Instanciate the Chips class.
        instance = new Chips(this, options);

        // Save on element.
        $this.data('bs.button', instance);
      }

      // If possible, call the method refered to by `arg`.
      if (typeof arg === 'string') {
        instance[method]();
      }
    });
  }

  var old = $.fn.chips;

  $.fn.chips             = Plugin;
  $.fn.chips.Constructor = Chips;

  /*
   * Chips plugin conflict prevention
   * --------------------------------
   */

  $.fn.chips.noConflict = function () {
    $.fn.chips = old;
    return this;
  };

  /*
   * Chips data-API
   * --------------
   */

  $(window).on('load.chips.data-api', function () {
    $('[data-spy="chips"]').each(function () {
      var $this = $(this);
      Plugin.call($this, $this.data());
    });
  });

})(window, jQuery);
