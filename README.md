chips.js
========

This library intends to provide the logic behind the implementation of the
EU-imposed warning regarding tracking cookies on website.

The approach used is not fully compliant since it doesn't let the user choose
so the text should be worded as *accept and proceed or leave*.

This library depends on jQuery.


Requirements
------------

- [jquery](https://github.com/jquery/jquery)
- [jquery-cookie](https://github.com/carhartl/jquery-cookie)


Usage
-----

Manual usage:

    <div id="cookie-consent-message" style="display: none;">
      <div>
        This site uses some unobtrusive cookies to store information on your
        computer. By continuing to use this website you give "implied" consent
        for these cookies to be stored.
      </div>

      <button data-dismiss="chips">
        OK
      </button>
    </div>

    <script>
      $('#cookie-consent-message').chips();
    </script>

Automatic usage:

    <div style="display: none;" data-spy="chips">
      <div>
        This site uses some unobtrusive cookies to store information on your
        computer. By continuing to use this website you give "implied" consent
        for these cookies to be stored.
      </div>

      <button data-dismiss="chips">
        OK
      </button>
    </div>


Options
-------

The following options can be both passed to `$.fn.chips` and through the
data API (eg. `data-cookie="monster"`):

- cookie: Cookie used to store the user's consent (`cookies-consent`)
- expires: Cookie consent expiration delay in days (`365`)
- auto: Automatically show after instantiation (`true`)
- delay: Delay before automatically showing (`750`)
- dismiss: Dismiss selector `[data-dismiss=chips]`)
- jQuery options for showing:
    - `showDuration`: `150`
    - `showEasing`: `linear`
    - `showMethod`: `slideDown`
- jQuery options for hiding:
    - `hideDuration`: `50`
    - `hideEasing`: `linear`
    - `hideMethod`: `slideUp`


License
-------

This software is provided under the ISC license; please see `LICENSE` for more
information.
